import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';

import Home from "./contents/Home";
import indoDetail from "./contents/indoDetail";
import worldDetail from "./contents/worldDetail";
import About from "./contents/About";



const HomeStack = createStackNavigator();
const HomeStackScreen = () => (
  <HomeStack.Navigator initialRouteName="HomeScreen">
    <HomeStack.Screen name="HomeScreen" component={Home} options={{ headerShown: false }} />
    <HomeStack.Screen name="indoDetail"component={indoDetail} options={{ headerShown: false }} />
    <HomeStack.Screen name="worldDetail"component={worldDetail} options={{ headerShown: false }} />
  </HomeStack.Navigator>
);



const Tabs = createBottomTabNavigator();

const MenuScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={HomeStackScreen} />
    <Tabs.Screen name="About" component={About} />
  </Tabs.Navigator>
);



const Stack = createStackNavigator();

export default () => {
  return (
    
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Register' component={RegisterScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Menu' component={MenuScreen} options={{ headerTitle: 'Covid-19' }} />
        </Stack.Navigator>
      </NavigationContainer>
   
  );
};
