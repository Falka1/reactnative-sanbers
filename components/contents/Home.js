import * as React from 'react';
import { Text, View, StyleSheet,ScrollView,ActivityIndicator,TouchableOpacity} from 'react-native';
import Axios from 'axios';


export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      indo: {},
      isLoading: true,
      isError: false
    };
  }

  componentDidMount() {
    this.GetApi()
  }


  GetApi = async () => {
    try {
      const response = await Axios.get(`https://covid19.mathdro.id/api/`)
      const response2 = await Axios.get(`https://covid19.mathdro.id/api/countries/indonesia`)
      this.setState({ isError: false, isLoading: false, data: response.data, indo: response2.data })
      console.log(this.state.indo)
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  
  render(){
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
  return (
    <View style={styles.container}>
    <ScrollView >
      <View style={styles.card}>
      <Text style={{fontSize: 28,fontWeight: 'bold',textAlign: 'center',}}>
          World Summary
        </Text>
        <Text style={{fontSize: 20,fontWeight: 'bold', color: '#e74c3c',}}>
          Confirmed
        </Text>
        <Text style={styles.text}>
          {this.state.data.confirmed.value}
        </Text>
        <Text style={{fontSize: 20,fontWeight: 'bold', color: '#2ecc71',}}>
          Recovered
        </Text>
        <Text style={styles.text}>
        {this.state.data.recovered.value}
        </Text>
        <Text style={{fontSize: 20,fontWeight: 'bold', color: '#34495e',}}>
          Deaths
        </Text>
        <Text style={styles.text}>
        {this.state.data.deaths.value}
        </Text>
        <TouchableOpacity 
              style={styles.btn} 
             onPress={() => this.props.navigation.navigate('worldDetail')}
              >
              <Text style={{fontSize: 12,color:'#fff'}}>Detail</Text>
        </TouchableOpacity>
    </View>
     <View style={styles.card}>
        <Text style={{fontSize: 22,fontWeight: 'bold',textAlign: 'center',}}>
          Indonesia
        </Text>
      <View  style={{flexDirection: 'row',justifyContent: 'space-evenly',marginTop: 8,}}>
        <View style={styles.card1}>
          <Text style={styles.text1}>
            Confirmed
          </Text>
          <Text>
          {this.state.indo.confirmed.value}
          </Text>
        </View>
        <View style={styles.card1}>
          <Text style={styles.text1}>
            Recovered
          </Text>
          <Text>
          {this.state.indo.recovered.value}
          </Text>
        </View>
        <View style={styles.card1}>
          <Text style={styles.text1}>
            Death
          </Text>
          <Text>
          {this.state.indo.deaths.value}
          </Text>
        </View>
      </View>
      <TouchableOpacity 
              style={styles.btn} 
             onPress={() => this.props.navigation.navigate('indoDetail')}
              >
              <Text style={{fontSize: 12,color:'#fff'}}>Detail</Text>
      </TouchableOpacity>
    </View>
    <View style={styles.card}>
      <Text style={{fontSize: 18,fontWeight: 'bold',textAlign: 'center',}}>CARA PENYEBARAN VIRUS INI</Text>
      <Text>Virus yang menyebabkan COVID-19 terutama ditransmisikan melalui droplet (percikan air liur) yang dihasilkan saat orang yang terinfeksi batuk, bersin, atau mengembuskan nafas. Droplet ini terlalu berat dan tidak bisa bertahan di udara, sehingga dengan cepat jatuh dan menempel pada lantai atau permukaan lainnya.</Text>
    </View>
    </ScrollView>
    </View>
  );
 }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 12,
    backgroundColor: '#2980b9',
    flex: 1,
  },
  text: {
    marginTop: 5,
    fontSize: 18,
  },
  text1: {
    fontSize: 15,
    fontWeight: 'bold', 
    color: '#fff'
  },
  card: {
    paddingTop: 20,
    marginTop: 10,
    margin: 20,
    padding: 20,
    alignSelf: 'stretch',
    borderRadius: 5,
    backgroundColor: '#fff',

  },
  card1: {
    margin: 2,
    alignSelf: 'stretch',
    alignItems: 'center',
    width: 82,
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#27ae60',

  },
  btn: {
    alignSelf:'stretch',
    padding:10,
    borderRadius:5,
    margin:10,
    backgroundColor: '#44bd32',
    alignItems: 'center',
  },

});
