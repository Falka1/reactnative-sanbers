import * as React from 'react';
import { Text, View, StyleSheet,ActivityIndicator,FlatList,} from 'react-native';
import Axios from 'axios';

export default class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  componentDidMount() {
    this.GetApi()
  }


  GetApi = async () => {
    try {
      const response = await Axios.get(`https://api.covid19api.com/summary`)
      this.setState({ isError: false, isLoading: false, data: response.data })
      console.log(this.state.data)
      console.log(this.state.data.Countries)
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render(){
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
  return (
    <View style={styles.container}>
      <Text style={{fontSize: 22,fontWeight: 'bold',textAlign: 'center', margin: 22, color:'white'}}>
          Detail Kasus Seluruh Dunia
        </Text>

      <FlatList
          data={this.state.data.Countries}
          renderItem={(data) => <ListItem data={data} />}
          keyExtractor={({ id }, index) => index.toString()}
          
        />
  
    </View>
  );
}
}

class ListItem extends React.Component {

  render() {
    const item = this.props.data.item;
    return (
      <View style={styles.card}>
              <Text style={{fontSize: 14,fontWeight: 'bold',textAlign: 'center',}}> {item.Country}</Text>
              <View style={{flexDirection: 'row',justifyContent: 'space-evenly',marginTop: 4,}}>
                <View>
                  <Text>Confirmed</Text>
                  <Text>{item.TotalConfirmed} </Text>
                </View>
                <View>
                  <Text>Recovered</Text>
                  <Text>{item.TotalRecovered} </Text>
                </View>
                <View>
                  <Text>Death</Text>
                  <Text>{item.TotalDeaths} </Text>
                </View>
              </View>
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2980b9',
    flex: 1,
  },
  paragraph: {
    margin: 24,
    marginTop: 0,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  card: {
    margin: 10,
    marginTop: 5,
    padding: 15,
    alignSelf: 'stretch',
    borderRadius: 5,
    backgroundColor: '#fff',
    width: 255

  }
});
