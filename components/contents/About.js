import * as React from 'react';
import { Text, View, StyleSheet, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function About() {
  return (
    <View style={styles.container}>
    <ScrollView>
    <View style={styles.head}>
      <Text style={{color: '#003366', fontSize: 28, fontWeight: 'bold',marginBottom:10,}}>About Us</Text>
      <Icon name="user-circle" size={120}/>
      <Text style={{color: '#003366', fontSize: 24, fontWeight: 'bold',marginTop:10}}>Danu Falka</Text>
      <Text style={{color: '#2980b9', fontSize: 18, fontWeight: 'bold',marginBottom:20,}}>
        React Native Developers
      </Text>
    </View>  
    <View style={styles.portofolio}>
      <Text style={{color: '#003366', fontSize: 14, fontWeight: 'bold',}}>Portofolio </Text>
      <View style={{height: 3,width: '100%',backgroundColor: '#909090',}}/>
      <View style={styles.portoicon}>
        <View style={{alignItems: 'center',}}>
        <Icon name="gitlab" size={50} color="#2980b9"/>
        <Text style={{color: '#2980b9', fontSize: 12, fontWeight: 'bold',marginTop:5}}>@Falka1</Text>
        </View>
        <View style={{alignItems: 'center',}}>
        <Icon name="github" size={50} color="#2980b9"/>
        <Text style={{color: '#2980b9', fontSize: 12, fontWeight: 'bold',marginTop:5}}>@danu_falka</Text>
        </View>     
      </View>
    </View>

    <View style={styles.portofolio}>
      <Text style={{color: '#003366', fontSize: 14, fontWeight: 'bold',}}>Hubungi Saya </Text>
      <View style={{height: 3,width: '100%',backgroundColor: '#909090',}}/>
      <View style={styles.sosmedicon}>
        <View style={{flexDirection: 'row',}}>
        <Icon name="facebook-square" size={40} color="#2980b9"/>
        <Text style={{color: '#2980b9', fontSize: 16, fontWeight:'bold',marginLeft:5}}>M Danu Syach Falka</Text>
        </View>
        <View style={{flexDirection: 'row',}}>
        <Icon name="instagram" size={40} color="#2980b9"/>
        <Text style={{color: '#2980b9', fontSize: 16, fontWeight:'bold',marginLeft:5}}>@danu_falka</Text>
        </View>  
        <View style={{flexDirection: 'row',}}>
        <Icon name="twitter" size={40} color="#2980b9"/>
        <Text style={{color: '#2980b9', fontSize: 16, fontWeight: 'bold',marginLeft:5}}>@danu_falka</Text>
        </View>    
      </View>
    </View>

    
  
    </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
    flex: 1,
    padding: 12,
    backgroundColor: '#2980b9',
  },
  head: {
    alignItems: 'center',
    marginBottom:20,
    backgroundColor: '#fff',
    borderRadius: 15,
  },
  portofolio: {
    marginBottom: 20,
    padding: 15,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderRadius: 15,
  },
  portoicon: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  sosmedicon: {
    marginTop: 10,
  }

});
