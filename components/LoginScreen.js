import React from 'react';
import {   View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput, } from 'react-native';



export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password)
    if( '12345678' === this.state.password){
        //alert('logged in');
        this.setState({isError:false})
        this.props.navigation.navigate('Menu')
      } else {
        this.setState({isError:true})
        alert('Password salah');
      }


  }

  render() {
    return (
       <View style={styles.container}>
        <Text onPress={console.log(this.props)} style={styles.judul}>
          Covid-19 Tracker
        </Text>
        <View style={styles.form}>
        
          <TextInput 
            placeholder="Username" 
            style={styles.input} 
             onChangeText={userName => this.setState({ userName })} 
             />
          <TextInput 
            placeholder="Password" 
            style={styles.input} 
             onChangeText={password => this.setState({ password })}
                secureTextEntry={true} />
          <View style={styles.grButton}>
            <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
            <TouchableOpacity 
              style={styles.btn} 
              //onPress={this.login}
             onPress={() => this.loginHandler()}
              >
              <Text style={{fontSize: 12,color:'#fff'}}>Login !</Text>
            </TouchableOpacity>
            
            <TouchableOpacity
              style={styles.register}
              onPress={() => this.props.navigation.navigate('Register')}>
              <Text style={{fontSize: 12,color:'#000'}}>belum terdaftar?</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
   container: {
    flex: 1,
    backgroundColor: '#2980b9',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft:40,
    paddingRight:40,
  },

  judul: {
    fontSize: 30,
    color: '#ffff',
    
  },

  form: {
    paddingTop: 50,
    marginTop: 20,
    marginBottom: 20,
    padding: 20,
    alignSelf: 'stretch',
    borderRadius: 5,
    backgroundColor: '#fff',

  },

  input: {
    alignSelf:'stretch',
    padding:10,
    borderRadius:25, 
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    marginBottom:20,
    backgroundColor: '#fff'
      },

  grButton: {
    flex : 1,
    alignContent : 'space-between'
    // flexBasis: 100,
  },
  btn: {
    alignSelf:'stretch',
    padding:10,
    borderRadius:25,
    marginBottom:20,
    backgroundColor: '#44bd32',
    alignItems: 'center',
  },
  register: {
    alignItems: 'center',
    padding:'40',
    color:'#fff'  
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});